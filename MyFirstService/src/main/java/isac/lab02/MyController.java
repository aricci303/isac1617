package isac.lab02;

import static javax.ws.rs.client.Entity.json;

import java.io.StringReader;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;


public class MyController {

	static MyController instance;
	private int reqCount;
	private Executor exec;
	
	public MyController(){
		exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	}
	
	public synchronized String manageRequest(double value){
		String id = ""+reqCount;
		reqCount++;
		exec.execute(() -> {
			try {
				log("[EXECUTOR] New task started - "+id);
				// System.out.println("[EXECUTOR] New task started - "+id);
				Thread.sleep(5000);
				double res = value*value;
				JsonObject result = Json.createObjectBuilder()
		        		.add("resId", id)
		        		.add("result", res)
		        		.build();			

				MyStore.getInstance().store(id, result);
				log("[EXECUTOR] Stored result - "+id+" -> \n"+ result);
				log("[EXECUTOR] New task done - "+id+" ");
			} catch (Exception ex){
				ex.printStackTrace();
			}
		});
		return id;
	}

	public synchronized String manageRequest(double value, String resURI){
		String id = ""+reqCount;
		reqCount++;
		exec.execute(() -> {
			try {
				log("[EXECUTOR] New task started - "+id);
				// System.out.println("[EXECUTOR] New task started - "+id);
				Thread.sleep(5000);
				double res = value*value;
				JsonObject result = Json.createObjectBuilder()
		        		.add("resId", id)
		        		.add("result", res)
		        		.build();			

				log("[EXECUTOR] Creating res in: "+resURI);
				Client client = ClientBuilder.newClient();
				WebTarget myRes = client.target(resURI);
				Invocation req = myRes.request(MediaType.APPLICATION_JSON).buildPost(json(result.toString()));
				String ret = req.invoke(String.class);

				// MyStore.getInstance().store(id, result);
				log("[EXECUTOR] RES POSTED - "+ret);
				log("[EXECUTOR] New task done - "+id+" ");
			} catch (Exception ex){
				ex.printStackTrace();
			}
		});
		return id;
	}
	
    static public MyController getInstance(){
		synchronized (MyController.class){
			if (instance == null){
				instance = new MyController();
			}
			return instance;
		}
	}

    private void log(String msg){
    	Logger.getGlobal().log(Level.INFO,msg);
    }

}
