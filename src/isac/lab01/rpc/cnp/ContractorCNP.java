package isac.lab01.rpc.cnp;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.concurrent.Semaphore;

import isac.lab01.rpc.HelloService;

public class ContractorCNP implements ContractorNotifications {

	private boolean taskAvailable;
	private boolean resultNotified;
	private Task task;
	private boolean awarded;
	
	public ContractorCNP() throws RemoteException {
		UnicastRemoteObject.exportObject(this, 0);
		taskAvailable = false;
		resultNotified = false;
	}
	
	public synchronized Task waitForTask() throws Exception {
		while (!taskAvailable){
			wait();
		}
		return task;
	}

	protected synchronized boolean waitForResult() throws Exception{
		while (!resultNotified){
			wait();
		}
		return awarded;
	}
	
	@Override
	public synchronized void notifyNewTask(Task t) throws RemoteException {		
		task = t;
		taskAvailable = true;
		notifyAll();
	}

	@Override
	public synchronized void notifyAwarded() throws RemoteException {
		awarded = true;
		resultNotified = true;
		notifyAll();
	}

	@Override
	public synchronized void notifyNotAwarded() throws RemoteException {		
		awarded = false;
		resultNotified = true;
		notifyAll();
	}
}
