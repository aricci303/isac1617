package isac.lab01.rpc.cnp;

import java.rmi.*;

public interface ContractorActions extends Remote {

	void bid(Bid bid) throws RemoteException;
	
	void register(ContractorNotifications c) throws RemoteException;
	
}
