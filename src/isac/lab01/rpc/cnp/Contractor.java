package isac.lab01.rpc.cnp;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.Semaphore;

public class Contractor extends Thread  {

	private String host;
	private ContractorActions cnp;
	private ContractorCNP myBoard;
	private final ContractorId id;
	private Task task;
	private int mybid;
	
	public Contractor(String name, int bid) throws RemoteException {
		id = new ContractorId(name);
		mybid = bid;
		myBoard = new ContractorCNP();
	}
	
	public void run(){
		try {
			log("Locating the board...");
			locateTheBoard();
			cnp.register(myBoard);
			log("Waiting for tasks...");
			myBoard.waitForTask();
			log("Received new task: "+task);
			Bid bid = new Bid(id, myBoard, mybid);
			log("Placing a bid: "+bid.getBid());
			cnp.bid(bid);
			
			log("Waiting for result...");
			boolean awarded = myBoard.waitForResult();
			if (awarded){
				log("Awarded!");
			} else {
				log("SOB! not awarded.");
			}			
			log("quit.");
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

	protected void locateTheBoard() throws RemoteException {
        Registry registry = LocateRegistry.getRegistry(host);
        cnp = null;
        boolean found = false;
		while (!found){
			try {
				 cnp = (ContractorActions) registry.lookup("CNP-Board");
				 found = true;
			} catch (Exception ex){
				waitFor(100);
			}
		}
	}
	
	protected void waitFor(int ms){
		try {
			sleep(ms);
		} catch (Exception ex){}
	}

	protected void log(String msg){
		synchronized (System.out){
			System.out.println("["+id.getName()+"] "+msg);
		}
	}
	
}
