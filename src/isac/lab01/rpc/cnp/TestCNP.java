package isac.lab01.rpc.cnp;

public class TestCNP {

	public static void main(String[] args) throws Exception {
		new Manager().start();		
		new Contractor("c1", 100).start();
		new Contractor("c2", 20).start();
		new Contractor("c3", 40).start();
	}

}
