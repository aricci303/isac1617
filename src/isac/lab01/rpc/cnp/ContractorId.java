package isac.lab01.rpc.cnp;

import java.io.Serializable;

public class ContractorId implements Serializable {
	
	private final String name;
	
	public ContractorId(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean equals(Object obj){
		return ((ContractorId) obj).getName().equals(name);
	}
	
	public String toString(){
		return name;
	}
}
