package isac.lab01.rpc.cnp;

import java.rmi.*;
import java.util.List;

public interface ManagerActions {

	void advertise(Task task);
	
	List<Bid> getBids();
		
}
