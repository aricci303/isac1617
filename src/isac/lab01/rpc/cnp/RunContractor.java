package isac.lab01.rpc.cnp;

public class RunContractor {

	public static void main(String[] args) throws Exception {
		new Contractor(args[0], Integer.parseInt(args[1])).start();
	}

}
