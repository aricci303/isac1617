package isac.lab01.rpc.cnp;

import java.rmi.*;

public interface ContractorNotifications extends Remote {

	void notifyNewTask(Task t) throws RemoteException;
	
	void notifyAwarded() throws RemoteException;
	
	void notifyNotAwarded() throws RemoteException;
}
