package isac.lab01.rpc;

import java.rmi.RemoteException;

public class HelloServiceImplExt extends HelloServiceImpl implements HelloServiceExt {
        
    public HelloServiceImplExt() {}

    
    public void sayHello(Message m) {
        System.out.println("hello: "+m.getContent());
    }
        
}