package isac.lab01.rpc;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
        
public class ServerExt  {
                
    public static void main(String args[]) {
        
        try {
            HelloServiceExt obj = new HelloServiceImplExt();
            HelloServiceExt stub = (HelloServiceExt) UnicastRemoteObject.exportObject(obj, 0);

            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            
            registry.rebind("HelloExt", stub);
            System.out.println("ServerExt ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}