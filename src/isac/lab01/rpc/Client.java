package isac.lab01.rpc;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Client {

    private Client() {}

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            HelloService stub = (HelloService) registry.lookup("Hello");
            
            Counter c = (Counter) registry.lookup("pippo");
            
            
            String response = stub.sayHello();
            System.out.println("response: " + response);
            System.out.println("response: " + stub.sayHello(10));

            int value = c.getValue();
            System.out.println("> value "+value);
            c.inc();
            System.out.println("> value "+c.getValue());
            

            HelloServiceExt helloServiceExt = (HelloServiceExt) registry.lookup("HelloExt");
            Message m = new Message("Cesena");
            helloServiceExt.sayHello(m);
            
            MyClass obj = new MyClass(300); 
            System.out.println("before: >> "+obj.get());
            UnicastRemoteObject.exportObject(obj, 0);
            response = stub.sayHello(obj);
            System.out.println("response: " + response);
            System.out.println("after: >> "+obj.get());
            
            
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}