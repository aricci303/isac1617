package isac.lab01.rpc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HelloServiceExt extends HelloService {
	
    void sayHello(Message n) throws RemoteException;

}