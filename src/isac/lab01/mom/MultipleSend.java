package isac.lab01.mom;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MultipleSend {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {
   
	ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    
    Channel channel = connection.createChannel();
    channel.queueDeclare(QUEUE_NAME, false, false, false, null);

    String message = "Hello World!";

    long t0 = System.currentTimeMillis();
    int n = 0;
    System.out.println("Start sending..");
    while (n < 100000){
    	String msg = message + " " + n;
    	channel.basicPublish("", QUEUE_NAME, null, msg.getBytes("UTF-8"));  
    	// System.out.println(" [x] Sent '" + message + "'");
    	n++;
    } 
    long t1 = System.currentTimeMillis();
    System.out.println("Task completed in "+(t1-t0)+" ms.");
    
    channel.close();
    connection.close();
  }
}
