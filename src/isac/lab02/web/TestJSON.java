package isac.lab02.web;

import java.net.*;
import java.io.*;
import javax.json.*;

public class TestJSON {

	public static void main(String[] args) throws Exception {
		 URL url = new URL("http://maps.googleapis.com/maps/api/directions/json?origin=Cesena&destination=Bologna&sensor=false");
		 try (InputStream is = url.openStream();
		       JsonReader rdr = Json.createReader(is)) {
		 
		      JsonObject obj = rdr.readObject();

		      System.out.println("RESPONSE:");
		      System.out.println(obj.toString());
		      		      
		      JsonArray routes = obj.getJsonArray("routes");
		      
		      System.out.println("ROUTES INFO");
		      for (JsonObject result: routes.getValuesAs(JsonObject.class)) {
		    	  System.out.print(result.getJsonObject("bounds"));
		     }
		}

	}
}
