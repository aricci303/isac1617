package isac.lab02.web;

import java.net.*;
import java.io.*;
import javax.json.*;

public class TestGoogleAPI {

	public static void main(String[] args) throws Exception {
		URL url = new URL("http://maps.googleapis.com/maps/api/directions/json?origin=Cesena&destination=Bologna&sensor=false");
		try {
			long t0 = System.currentTimeMillis();
			InputStream is = url.openStream();
			JsonReader rdr = Json.createReader(is);
			JsonObject obj = rdr.readObject();
			is.close();
			long t1 = System.currentTimeMillis();
			System.out.println("Result fetched in "+(t1-t0)+" ms");
			
			JsonArray routes = obj.getJsonArray("routes");
			for (JsonObject result : routes.getValuesAs(JsonObject.class)) {
				JsonObject bounds = result.getJsonObject("bounds");
				System.out.println("Bounds:");
				System.out.println(bounds.getJsonObject("northeast"));
				System.out.println(bounds.getJsonObject("southwest"));
				
				JsonObject leg = result.getJsonArray("legs").getValuesAs(JsonObject.class).get(0);
				JsonNumber distance = leg.getJsonObject("distance").getJsonNumber("value");
				System.out.println("Distance: "+distance.longValue());
				
				JsonArray steps = leg.getJsonArray("steps");
				System.out.println("Num steps: "+steps.size());
				
				for (JsonObject step : steps.getValuesAs(JsonObject.class)) {
					System.out.println("From: "+step.getJsonObject("start_location"));
					System.out.println("To: "+step.getJsonObject("end_location"));
					System.out.println("--");
				}
			}
		} catch (Exception ex){
			ex.printStackTrace();
		} finally {

		}

	}
}
