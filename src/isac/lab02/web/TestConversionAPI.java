package isac.lab02.web;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.*;
import org.apache.http.entity.mime.content.*;
 
public class TestConversionAPI {
  
	/**
	 * This example is working with a temporary API key 
	 * got at https://www.mashape.com/convertapi/text2pdf
	 * with an APP key = isac1415
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
  
		String url = "https://convertapi-text2pdf.p.mashape.com/?ApiKey=853159251";
		URL obj = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

		conn.setRequestProperty ("X-Mashape-Key", "tT9UcqKl6tmsh4CYGNw20XPVjp4kp11PaL2jsnDiGoPhcLlyrR");

		String txtFilePath = "data/test.txt";
		if (!(new File(txtFilePath).isFile())){
			System.err.println("File not found.");
			System.exit(1);
		}
		
		System.out.println("\nSending request to " + url);

		/* specifying the header info */
		conn.setRequestMethod("POST");
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
		FileBody fileBody = new FileBody(new File(txtFilePath)); 
		builder.addPart("File", fileBody); 		
		HttpEntity  reqEntity = builder.build();
		    
		// Send post request
		conn.setDoOutput(true);
	    conn.setRequestProperty("Connection", "Keep-Alive");
        conn.addRequestProperty("Content-length", reqEntity.getContentLength()+"");
        conn.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());        
        OutputStream os = conn.getOutputStream();
        reqEntity.writeTo(conn.getOutputStream());
        os.close();
        conn.connect();
 
		int responseCode = conn.getResponseCode();
		System.out.println("Response Code : " + responseCode);
 
		
		BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
		FileOutputStream pdfFile = new FileOutputStream("data/test.pdf");
		byte[] buffer = new byte[1024*1024];
		while (true){
			int nread = in.read(buffer);
			if (nread > 0){
				pdfFile.write(buffer, 0, nread);
			} else {
				break;
			}
		}
		in.close();
		pdfFile.close();
 
		System.out.println("Ok.");
		 
	} 

}