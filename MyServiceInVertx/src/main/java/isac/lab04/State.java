package isac.lab04;

public class State {

	private int count;
	
	public State(){
		count = 0;
	}
	
	public void update(){
		count++;
	}
	
	public int getCount(){
		return count;
	}
}
